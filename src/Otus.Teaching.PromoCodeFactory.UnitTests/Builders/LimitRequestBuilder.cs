﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class LimitRequestBuilder
    {
        public static SetPartnerPromoCodeLimitRequest GetNormalLimitRequest()
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                EndDate = new DateTime(2020, 11, 30),
                Limit = 1
            };
        }

        public static SetPartnerPromoCodeLimitRequest GetNegativeRequest(this SetPartnerPromoCodeLimitRequest request)
        {
            request.Limit = -1;
            return request;
        }

        public static SetPartnerPromoCodeLimitRequest GetLimitEndingToDay(this SetPartnerPromoCodeLimitRequest request)
        {
            request.EndDate = DateTime.Today;
            return request;
        }
    }
}
