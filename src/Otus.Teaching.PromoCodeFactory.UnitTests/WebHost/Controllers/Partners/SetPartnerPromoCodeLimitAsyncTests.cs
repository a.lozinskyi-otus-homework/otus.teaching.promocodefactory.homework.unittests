﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private const string PartnerIdStr = "{DC58FD93-249C-EA11-80D5-00155D640A42}";
        private readonly Mock<IRepository<Partner>> _partnersRepoMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepoMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse(PartnerIdStr);
            Partner partner = null;
            _partnersRepoMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, GetBaseLimitRequest());

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData(PartnerIdStr)]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotActive_ReturnsBadRequest(string parnterIdstr)
        {
            // Arrange
            var partnerId = Guid.Parse(parnterIdstr);
            var partner = GetBasePartner();
            partner.IsActive = false;
            _partnersRepoMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, GetBaseLimitRequest());

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_ActiveLimit_SetIssuedPromocodesToZero()
        {
            // Arrange
            var partner = GetBasePartner().WithOnlyOneActiveLimit();
            partner.NumberIssuedPromoCodes = 3;
            _partnersRepoMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, GetBaseLimitRequest());

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_NonActiveLimit_IssuedPromocodesNumberNotChanged()
        {
            var issuedPromocodesNumber = 3;
            // Arrange
            var partner = GetBasePartner().WithNoActiveLimits();
            partner.NumberIssuedPromoCodes = issuedPromocodesNumber;

            _partnersRepoMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, GetBaseLimitRequest());

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(issuedPromocodesNumber);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_HasActiveLimit_PreviousLimitCancelled()
        {
            // Arrange
            var partner = GetBasePartner().WithOnlyOneActiveLimit();
            var previousLimitId = partner.PartnerLimits.FirstOrDefault().Id;
            
            _partnersRepoMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, GetBaseLimitRequest());

            //Assert
            var previousLimit = partner.PartnerLimits.FirstOrDefault(l => l.Id == previousLimitId);
            previousLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_LimitLessThanZero_ReturnsBadRequest()
        {
            // Arrange
            var partner = GetBasePartner().WithOnlyOneActiveLimit();
            _partnersRepoMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, GetBaseLimitRequest().GetNegativeRequest());

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_NewLimit_IsSavedInDb()
        {
            // Arrange
            var partner = GetBasePartner();
            _partnersRepoMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, GetBaseLimitRequest());

            //Assert
            _partnersRepoMock.Verify(repo => repo.UpdateAsync(partner), Times.Once);
        }

        private Partner GetBasePartner()
        {
            return PartnerBuilder.CreateBasePartner();
        }

        private SetPartnerPromoCodeLimitRequest GetBaseLimitRequest()
        {
            return LimitRequestBuilder.GetNormalLimitRequest();
        }
    }
}